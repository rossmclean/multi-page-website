# Kilner Bank - E-Commerce Landing Page

Using HTML5 and CSS3, I designed this website as a template landing page for a potential e-commerce store.  

I used CSS Grid to display collections and products in a responsive and user-friendly way and made sure that the site would appear well on all types of device and web browser. 

The challenging aspect of this project was to ensure that the site didn't lose any of it's functionality or character based on the screen size which it was being displayed. For this I implemented CSS media queries and styled elements accordingly, using viewport-specific attributes for their sizing. I was pleased with the results on different device sizes. 

